#!/bin/sh

linter_api_url='https://gitlab.com/api/v4/ci/lint'

linter_dump_conf() {
  python -c "import json,sys;print(json.dumps({'content':sys.stdin.read()}))" < .gitlab-ci.yml
}

curl -sH 'Content-Type: application/json' "$linter_api_url" -d "$(linter_dump_conf)" | jq


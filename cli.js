#!/usr/bin/env node

const argv = require('yargs').argv;
const chalk = require('chalk');
const fs = require('fs');
const request = require('request');

function die(message) {
  console.error(chalk.red(message));
  process.exit(1);
}

const file = argv.file || '.gitlab-ci.yml';
if (!fs.existsSync(file)) {
  die('File ' + chalk.bold(file) + " doesn't exist.")
}

request.post({
  url: 'https://gitlab.com/api/v4/ci/lint',
  json: {
    content: fs.readFileSync(file).toString()
  }
}, function (err, res, body) {
  // If couldn't connect to GitLab.com server
  if (err || !body) {
    die("Couldn't connect GitLab.com server. Try again later.");
  }
  // If the file is illegally formatted
  if (body.error === 'content is invalid') {
    die('File ' + chalk.bold(file) + ' is in an invalid format.');
  }
  const errorLen = body.errors.length;
  // If the file contains some errors
  if (errorLen > 0) {
    console.error(chalk.red('File ' + chalk.bold(file) + ' contains ' + errorLen + ' error(s) which must be corrected.'));
    body.errors.forEach(function(message) {
      console.error('['+chalk.red('!')+'] ' + message);
    });
    process.exit(1)
  }
  // If the file is valid
  console.log(chalk.green('Everything looks all right!'));
});

#!/bin/sh

basedir="`dirname $0`"

test() {
  node "${basedir}/../cli.js" "--file=${basedir}/fixtures/${1}"
  return $?
}

{ echo 'gitlab-ci.no-errors.yml) should pass linter check'; test gitlab-ci.no-errors.yml; } && \
    { echo 'gitlab-ci.errors.yml) should detect an error'; ! test gitlab-ci.errors.yml; }

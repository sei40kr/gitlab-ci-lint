# gitlab-ci-lint
Lint your `.gitlab-ci.yml` easier.
## Installation
```bash
npm install -g git+ssh://git@gitlab.com:sei40kr/gitlab-ci-lint.git
```
## Usage
```bash
gitlab-ci-lint
gitlab-ci-lint --file=submodule/.gitlab-ci.yml
```
